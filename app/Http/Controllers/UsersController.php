<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use View;


class UsersController extends Controller
{
    public function index()
    {
		$customers = \App\suppliers::all();
        return view('users' , compact('customers'));
    }
}
